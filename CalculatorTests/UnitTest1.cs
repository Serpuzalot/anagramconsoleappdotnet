﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.IO;
using Task5.Calculator;

namespace CalculatorTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetResult_10_plus_10_20returned()
        {
            //arrange
            string expression = "10+10";
            double expected = 20;

            //act
            Calculator calc = new Task5.Calculator.Calculator(expression);
            double actual = calc.GetResult();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetResult_emtpy_string_PositiveInfinityreturned()
        {
            //arrange 
            string expression = "";
            double expected = double.PositiveInfinity;

            //act
            Calculator calc = new Task5.Calculator.Calculator(expression);
            double actual = calc.GetResult();

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]

        public void GetResult_sequence_check_6returned()
        {
            //arrange
            string expression = "2+2*2";
            double expected = 6;
            //act
            Calculator calc = new Task5.Calculator.Calculator(expression);
            double actual = calc.GetResult();

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]

        public void GetResult_wrong_signs_in_line_PositiveInfinityreturned()
        {
            //arrange
            string expression = "2+x-3";
            double expected = double.PositiveInfinity;

            //act
            Calculator calc = new Task5.Calculator.Calculator(expression);
            double actual = calc.GetResult();

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]

        public void CalculateFromFile_sequence_check_with_brecket_from_file()
        {
            //arrange
            string readerFilePath = "..//..//TestsFile//expressions.txt";
            string writerFilePath = "..//..//TestsFile//solutions.txt";
            ArrayList expectedList = new ArrayList();
            expectedList.Add(10);
            expectedList.Add(8);
            expectedList.Add(-1);
            bool expected = true;

            //act
            CalculateFromFile fromFile = new Task5.Calculator.CalculateFromFile(readerFilePath, writerFilePath);
            StreamReader reader = new StreamReader(writerFilePath);
            ArrayList actualList = new ArrayList();
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                bool switcher = false;
                string newLine = "";
                for(int i = 0; i < line.Length; i++)
                {
                    if (line[i] == '=' && !switcher)
                    {
                        switcher = true;
                        i+=2;
                    }
                    if (switcher)
                    {
                        newLine += line[i];
                    }
                }
                actualList.Add(newLine);
            }
            bool actual = true;
            for(int i = 0; i < expectedList.Count; i++)
            {
                if(actualList[i].ToString() != expectedList[i].ToString())
                {
                    actual = false;
                    break;
                }
            }

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]

        public void GetResult_sequence_check_with_brecket()
        {
            //arrange
            string expresion = "(2+2)*2";
            double expected = 8;

            //act
            Calculator calculator = new Task5.Calculator.Calculator(expresion);
            double actual = calculator.GetResult();

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]

        public void GetResult_devid_by_zero_NegativeInfinityreturned()
        {
            //arrange
            string expression = "2/0";
            double expected = double.NegativeInfinity;

            //act
            Calculator calc = new Task5.Calculator.Calculator(expression);
            double actual = calc.GetResult();

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]

        public void GetResult_5plus_PositiveInfinityreturned()
        {
            //arrange
            string expression = "5+";
            double expected = double.PositiveInfinity;

            //act
            Calculator calc = new Task5.Calculator.Calculator(expression);
            double actual = calc.GetResult();

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]

        public void GetResult_5xplus_1_PositiveInfinityreturned()
        {
            //arrange
            string expression = "5x+1";
            double expected = double.PositiveInfinity;

            //act
            Calculator calc = new Task5.Calculator.Calculator(expression);
            double actual = calc.GetResult();

            //assert
            Assert.AreEqual(expected, actual);

        }

    }
}
