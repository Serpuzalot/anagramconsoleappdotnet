﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5.Calculator
{
    public class CalculateFromFile
    {
        private string filePathRead;
        private string filePathWrite;

        public  CalculateFromFile(string filePathRead, string filePathWrite)
        {
            this.filePathRead = filePathRead;
            this.filePathWrite = filePathWrite;
            if (!File.Exists(filePathRead))
            {
                return;
            }
            StreamWriter fwrite = new StreamWriter(filePathWrite);
            fwrite.Close();
            StreamReader reader = new StreamReader(filePathRead);
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                Calculator calculator = new Calculator(line);
                FileWriter(calculator.GetResult(), line);
            }
        }

        void FileWriter(double result,string line)
        {
            StreamWriter writer = new StreamWriter(filePathWrite, true);
            if (result == double.PositiveInfinity)
            {
                writer.WriteLine(line + " = Expression input error");
            }
            else if (result == double.NegativeInfinity)
            {
                writer.WriteLine(line + " = Can`t be divided by zero");
            }
            else
            {
                writer.WriteLine(line + " = " + result); ;
            }
            writer.Close();
        }
    }
}
