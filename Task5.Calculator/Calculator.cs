﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5.Calculator
{
    public class Calculator
    {
        private char[] _operationsOrder = {'*','/','+','-' };
        private Dictionary<int, double> _values = new Dictionary<int, double>();
        private Dictionary<int,char> _signs = new Dictionary<int,char>();
        private int _equationLength;
        private string _expression;
        NumberStyles _style = NumberStyles.Number;
        CultureInfo _info = CultureInfo.CreateSpecificCulture("en-GB");

        public Calculator(string line)
        {
            _expression = line;
             
        }

        void Fill_values_and_signs()
        {
            string newLine = GetCorrectLine(_expression);
            if (newLine.Contains("("))
            {
                newLine = GetLineWithoutBracket(newLine);
            }
            _equationLength = newLine.Length;
            string value = "";
            int counter = 0;
            if (newLine == "")
            {
                return;
            }
            for (int i = 0; i < newLine.Length; i++)
            {
                if (i == 0)
                {
                    if (newLine[0] == '-' && Char.IsDigit(newLine[i + 1]))
                    {
                        value += newLine[0];
                        value += newLine[1];
                        i += 2;
                        if (i == newLine.Length)
                        {
                            _values.Add(counter, double.Parse(value, _style, _info));
                            return;
                        }
                    }
                }
                while (Char.IsDigit(newLine[i]) && i <= newLine.Length - 1 || newLine[i] == '.')
                {
                    if (i == newLine.Length - 1)
                    {
                        value += newLine[i];
                        break;
                    }
                    value += newLine[i];
                    i++;
                }
                if (value != "")
                {
                    _values.Add(counter, double.Parse(value, _style, _info));
                    counter++;
                    value = "";
                }
                for (int j = 0; j < _operationsOrder.Length; j++)
                {
                    if (newLine[i] == _operationsOrder[j])
                    {
                        _signs.Add(counter, _operationsOrder[j]);
                        counter++;
                        break;
                    }
                }

            }
        }

        string GetLineWithoutBracket(string line)
        {
            List<int> openBracket = new List<int>();
            List<int> closeBracket = new List<int>();
            List<string> lineList = new List<string>();
            int counter = 0;
            for(int i = 0; i < line.Length; i++)
            {
                if (line[i] == '(')
                {
                    counter++;
                }else if(line[i] == ')')
                {
                    counter--;
                }
                lineList.Add(line[i].ToString());
            }
            if(counter !=0)
            {
                return "";
            }
            for(int i = 0; i < lineList.Count; i++)
            {
                if(lineList[i] == "(")
                {
                    openBracket.Add(i);
                }else if(lineList[i] == ")")
                {
                    closeBracket.Add(i);
                }
                if(openBracket.Count == closeBracket.Count && openBracket.Any())
                {
                    string partOfLine = "";
                    for(int j = openBracket[0] ; j <= closeBracket[closeBracket.Count-1]; j++)
                    {
                        partOfLine += lineList[j];   
                    }
                    lineList[openBracket[0]] = ReturnResultFromPartOfLine(partOfLine);
                    i = openBracket[0];
                    lineList.RemoveRange(openBracket[0]+1,closeBracket[closeBracket.Count-1] - openBracket[0]);
                    openBracket.Clear();
                    closeBracket.Clear();
                }
            }
            string lineWithoutBracket = "";
            for(int i = 0; i < lineList.Count; i++)
            {
                lineWithoutBracket += lineList[i];
            }
            return lineWithoutBracket;
            
        }

        string ReturnResultFromPartOfLine(string partOfLine)
        {
            List<int> openBracket = new List<int>();
            List<int> closeBracket = new List<int>();
            List<string> partOfLineList = new List<string>();
            for(int i = 0; i < partOfLine.Length; i++)
            {
                if(partOfLine[i] == '(')
                {
                    openBracket.Add(i);
                }else if(partOfLine[i] == ')')
                {
                    closeBracket.Add(i);
                }
                partOfLineList.Add(partOfLine[i].ToString());
            }
            while (partOfLineList.Contains("("))
            {
                string expressionInBracket = "";
                if (openBracket.Count == 1)
                {
                    for (int i = openBracket[0] + 1; i < closeBracket[0]; i++)
                    {
                        expressionInBracket += partOfLineList[i];
                    }
                }
                else
                {
                    for (int i = openBracket[openBracket.Count - 1] + 1; i < closeBracket[0]; i++)
                    {
                        expressionInBracket += partOfLineList[i];
                    }
                }
                Calculator calc = new Calculator(expressionInBracket);
                partOfLineList[openBracket[openBracket.Count-1]] = calc.GetResult().ToString();
                partOfLineList.RemoveRange(openBracket[openBracket.Count-1]+1,closeBracket[0]-openBracket[openBracket.Count-1]);
                if (closeBracket.Count > 1)
                {
                    for (int i = 1; i < closeBracket.Count; i++)
                    {
                        closeBracket[i] -= closeBracket[0] - openBracket[openBracket.Count - 1];
                    }
                }
                openBracket.Remove(openBracket[openBracket.Count - 1]);
                closeBracket.Remove(closeBracket[closeBracket.Count - closeBracket.Count]);
            }
            string partOfLineWithoutBracket = "";
            for (int i = 0; i < partOfLineList.Count; i++)
            {
                partOfLineWithoutBracket += partOfLineList[i];
            }
            return partOfLineWithoutBracket;
        }

        string GetCorrectLine(string line)
        {
            string result = "";
            if (string.IsNullOrEmpty(line))
            {
                return "";
            }
            for (int i = 0; i < line.Length; i++)
            {
                if (Char.IsLetter(line[i]))
                {
                    return "";
                }
            }
            if (line[0] == '*' || line[0] == '/')
            {
                return "";
            }
            if (line[0] == '+')
            {
                for(int i = 1; i < line.Length; i++)
                {
                    result = line[i].ToString();
                }
                return result;
            }
            else if (Char.IsDigit(line[0]) || line[0] == '-'|| line[0] == '(')
            {
               return  line;
            }

            return result;
        }

        void RestructSigns(int key)
        {
            _signs.Remove(key);
            char temp;
            for(int i = 0; i < _equationLength; i++)
            {
                if (_signs.ContainsKey(i))
                {
                    if (i > key)
                    {
                        temp = _signs[i];
                        _signs.Remove(i);
                        _signs.Add(i - 2,temp);
                    }
                }
            }
        }

        void RestructValues(int key,double value)
        {
            if(key == 0)
            {
                _values.Remove(key + 1);
                _values.Remove(key);
                _values.Add(key, value);
            }
            else
            {
                _values.Remove(key + 1);
                _values.Remove(key - 1);
                _values.Add(key - 1, value);
            }
            
            double temp;
            for(int i = 0; i < _equationLength; i++)
            {
                if (_values.ContainsKey(i))
                {
                    if (i > key)
                    {
                        temp = _values[i];
                        _values.Remove(i);
                        _values.Add(i - 2,temp);
                    }
                }
            }
        }

        bool Calculate(char sign,int switcher)
        {
            double result = 0;
            int i = 0;
            while (_signs.ContainsValue(sign))
            {
                if (_signs.ContainsKey(i))
                {
                    if (_signs[i] == sign)
                    {
                        switch (switcher)
                        {
                            case 0:
                                result = _values[i - 1] * _values[i + 1];
                                break;
                            case 1:
                                if (_values[i + 1] == 0)
                                {
                                    return false;
                                }
                                result = _values[i - 1] / _values[i + 1];
                                break;
                            case 2:
                                result = _values[i - 1] + _values[i + 1];
                                break;
                            case 3:
                                result = _values[i - 1] - _values[i + 1];
                                break;
                           
                        }
                        RestructSigns(i);
                        RestructValues(i, result);
                    }
                }
                i++;
                if (i == _equationLength-1)
                {
                    i = 0;
                }
            }
            return true;   
            
        }

        public double GetResult()
        {
            Fill_values_and_signs();
            if (!_values.Any() || _values.Count != _signs.Count+1 || _values.Any() && !_signs.Any() && _values.Count!=1)
            {
                return double.PositiveInfinity;
            }
            int i = 0;
            while (i < 4)
            {
                bool isError = Calculate(_operationsOrder[i], i);
                if (!isError)
                {
                    return double.NegativeInfinity;
                }
                i++;
            }
            return _values[0];
        }

    }
}
