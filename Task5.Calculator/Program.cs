﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5.Calculator
{
    class Program
    {

        static int Menu()
        {
            Console.WriteLine("Enter opertion:\n1.Calculator\n2.Calculate from file\n3.Exit");
            string temp = Console.ReadLine();
            int choice;
            bool isError = int.TryParse(temp, out choice);
            while(!isError || choice >3 || choice < 1)
            {
                Console.WriteLine("Input opertion error,try again!");
                temp = Console.ReadLine();
                isError = int.TryParse(temp, out choice);
            }
            return choice;
        }

        static string[] GetFilePaths(string[]args)
        {
            string[] filePaths = new string[2];
            if (args.Any())
            {
                if(File.Exists(args[0]) && File.Exists(args[1]))
                {
                    filePaths[0] = args[0];
                    filePaths[1] = args[1];
                    return filePaths;
                }
            }
            else
            {
                Console.WriteLine("Enter file path for reading:");
                filePaths[0] = Console.ReadLine();
                while (!File.Exists(filePaths[0]))
                {
                    Console.WriteLine("Error,file don`t exist,try again.\nEnter file path for reading:");
                    filePaths[0] = Console.ReadLine();
                }
                Console.WriteLine("Enter file path for writing:");
                filePaths[1] = Console.ReadLine();
                return filePaths;
            }
            return filePaths;
        }

        static void Main(string[] args)
        {
            bool stopButton = false;
            while (!stopButton)
            {
                switch (Menu())
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Enter your expression:");
                        string expression = Console.ReadLine();
                        Calculator calculator = new Calculator(expression);
                        double result = calculator.GetResult();
                        if (result == double.PositiveInfinity)
                        {
                            Console.WriteLine("Result: " + expression + " = Expression input error");
                        }
                        else if (result == double.NegativeInfinity)
                        {
                            Console.WriteLine("Result: " + expression + " = Can`t be divided by zero");
                        }
                        else
                        {
                            Console.WriteLine("Result: " + expression + " = " + result); ;
                        }
                        break;
                    case 2:
                        Console.Clear();
                        string[] filePaths = GetFilePaths(args);
                        CalculateFromFile fromFile = new CalculateFromFile(filePaths[0], filePaths[1]);
                        break;
                    case 3:
                        Console.Clear();
                        stopButton = true;
                        break;
                }
            }
            
        }
    }
}
